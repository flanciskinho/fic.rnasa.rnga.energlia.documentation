% 
% PFC CLASS 
%
% Parameters:
%    hyper -> enable hyperlinks by means of hyperref pkg
% 
% built on *book* class, keeping these defaults:
%    twoside   -> using both sides of the physical paper
%    onecolumn -> only one column per page%
%    titlepage -> individual title page
%    openright -> chapters start in an odd (right) page
%
% and overriding these other params:
%    a4paper   -> A4 as default paper size
%    12pt      -> default font size
%

%% Class options. Book class used as base
\newif\if@hyper                       % Use Hyperlinks
\DeclareOption{hyper}{\@hypertrue}
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{book}}
\ProcessOptions
%                                     % Main format issues
\LoadClass[a4paper, 12pt, twoside, onecolumn, openright, titlepage]{book}

%% Indicamos que el fichero está codificado usando UTF-8.
\usepackage[utf8]{inputenc}

%% En castellano
\usepackage[spanish]{babel}

% Paquete que permite el uso de comentarios multilinea.
\usepackage{verbatim} 

% Paquete que contiene el simbolo del euro.
\usepackage{eurosym}

% Paquete para rotar elementos.
\usepackage{rotating}

% Paquetes para trabajar con tablas.
\usepackage{multicol}
\usepackage{multirow}

% Paquete para insertar fragmentos de código con resaltado de sintaxis.
\usepackage{listings}

%% Font encoding selection
% \usepackage{type1ec}
\usepackage{lmodern}      %% Computer Modern fonts in T1 encoding 
\usepackage[T1]{fontenc}  %% T1 Encoding (alternative in latin: OT1 - Old T1)


%% The page (A4 size)
\ifx\pdfoutput\undefined\else
\setlength{\pdfpageheight}{297mm}
\setlength{\pdfpagewidth}{210mm}
\fi
\setlength{\paperheight}{297mm}
\setlength{\paperwidth}{210mm}
\setlength{\textheight}{21cm}
\setlength{\textwidth}{15cm}
\setlength{\oddsidemargin}{1cm}
\setlength{\evensidemargin}{0cm}
%\setlength{\headheight}{2.0cm}
%\setlength{\headsep}{0.8cm}
%\setlength{\topskip}{12pt}
%\setlength{\footskip}{30pt}
%\setlength{\topmargin}{-2.0cm}
\setlength{\parskip}{0.5\baselineskip}

% Spacing...
 \usepackage{setspace}

%% Hyperlinks
\usepackage[colorlinks=true,linkcolor=blue,citecolor=magenta, urlcolor=red]{hyperref}
\if@hyper\IfFileExists{hyperref.sty}{\RequirePackage{hyperref}}{}\fi


%% Headings and footnotes
\usepackage{fancyhdr}
\pagestyle{fancy}

% with this we assure that the chapter and section
% headings are in lowercase
\renewcommand{\chaptermark}[1]{\markboth{\chaptername\ \thechapter. #1}{}}
%\renewcommand{\chaptermark}[1]{\markboth{#1}{}}
\renewcommand{\sectionmark}[1]{\markright{\thesection\ #1}}
\renewcommand{\baselinestretch}{1.1}

\fancyhf{}   % delete current setting for header and footing
%\fancyhead[EL, OR]{\bfseries\thepage} % bold pagenambers in headings 
%\fancyhead[ER]{\bfseries\leftmark} % bold chapter name
%\fancyhead[OL]{\bfseries\rightmark} % bold section name
\fancyhead[EL, OR]{\thepage} % pagenambers in headings 
\fancyhead[ER]{\leftmark} % chapter name
\fancyhead[OL]{\rightmark} % section name
%\addtolength{\headwidth}{\marginparsep}
%\addtolength{\headwidth}{\marginparwidth}

% avoids headings on otherwise empty pages before a new chapter
\def\cleardoublepage{\clearpage\if@twoside\ifodd\c@page\else
   \hbox{}\thispagestyle{empty}\newpage
   \if@twocolumn\hbox{}\newpage\fi\fi\fi}

%% list items
\renewcommand{\theenumi}{\arabic{enumi}.}
\renewcommand{\theenumii}{\theenumi\arabic{enumii}.}
\renewcommand{\theenumiii}{\theenumii\arabic{enumiii}.}
\renewcommand{\labelenumi}{\theenumi}
\renewcommand{\labelenumii}{\theenumii}
\renewcommand{\labelenumiii}{\theenumiii}

%% Info for title page
\def\department#1{\def\@department{#1}}   \def\@department{???}
\def\faculty#1{\def\@faculty{#1}}         \def\@faculty{???}
\def\university#1{\def\@university{#1}}   \def\@university{???}
\def\degree#1{\def\@degree{#1}}           \def\@degree{???}
\def\advisor#1{\def\@advisor{#1}}         \def\@advisor{???}
\def\logo[#1]#2{\def\@rotatelogo{#1} \def\@logo{#2}}           
                                          \def\@rotatelogo{0} \def\@logo{???}
%\def\logo#1{\def\@logo{#1}}               
\def\undef{???}

% Meses en castellano
\def\themonth{\ifcase\month\or
  Enero\or Febrero\or Marzo\or Abril\or Mayo\or Junio\or
  Julio\or Agosto\or Septiembre\or Octubre\or Noviembre\or Diciembre\fi}

%% Title page definition
\def\maketitle{%
  \begin{titlepage}
    
    % Upper space
%    \vspace*{1mm}
    
    \begin{center}
      
      % Affiliation
      {\Large 
        \@department\\[1mm]
        \@university\\
      }
      
      % Affiliation - logo space
      \vspace*{10mm}
      
      % Logo
      \ifx\@logo\undef\else
        \includegraphics[width=0.35\linewidth, angle=\@rotatelogo]{\@logo}
      \fi
      
      % Logo - title space
      \vspace*{15mm}
      
      % Type of document: PhD Thesis...
      {\Large \scshape \@degree\\[10mm]}
      
      % At least... the title
      {\Huge\bfseries\@title\par}
      
      % Title - name space
      \vspace{30mm}
      
      % Author's name
      {\large \@author\\[5mm]}
      
      % Date
      {\large \themonth\  de \number\year}\\[27.5mm]
    \end{center}
    
    % Advisor
    \ifx\@advisor\undef\else
    \begin{flushright}
      \begin{minipage}{\textwidth}
        \begin{tabbing}
          Dirigida por:\\
          \@advisor
        \end{tabbing}
      \end{minipage}
    \end{flushright}
    \fi
    
  \end{titlepage}
}


%%% Cliche page definition (pretty title page :-)
\def\makecliche{%
  \begin{titlepage}
    
    % Upper space
    \vspace*{1mm}

    % Title
    {\centering\Huge\fontfamily{pag}\selectfont\@title\par}

    % rule
    \centerline{\rule{0.9\textwidth}{.25mm}}

    \vspace*{.5cm}

    % Author's name
    {\centering \large \fontfamily{pag}\selectfont \itshape \@author\\[5mm]}

    % Logo - title space
    \vspace*{10cm}
    
    \begin{center}
      % Logo
      \ifx\@logo\undef\else
      \includegraphics[width=0.2\linewidth, angle=\@rotatelogo]{\@logo}
      \fi
      
      % Affiliation
      {\Large      
        \@department\\[0.5mm]
        \@university\\
      }
    \end{center}
    
  \end{titlepage}
}


\usepackage{fancybox}


% Maths
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{amsmath}

% Figures and subfigures
\usepackage{graphicx}
%\usepackage{subfig}
\usepackage{subfigure}


%% Enhanced tables
\usepackage{tabularx}
\renewcommand{\tabularxcolumn}[1]{m{#1}}
\usepackage{booktabs}
\usepackage{colortbl}

\usepackage{color}
\definecolor{light}{gray}{.75}
\definecolor{dark}{gray}{.50}
\definecolor{gray75}{gray}{.75}
\definecolor{gray97}{gray}{.97}
\definecolor{gray75}{gray}{.75}
\definecolor{gray45}{gray}{.45}

 
% minimizar fragmentado de listados
\lstnewenvironment{listing}[1][]
{\lstset{#1}\pagebreak[0]}{\pagebreak[0]}

\usepackage[usenames,dvipsnames]{xcolor}
\colorlet{keyword}{blue!100!black!80}
\colorlet{STD}{Lavender}
\colorlet{comment}{green!80!black!90}

 
\lstdefinestyle{consola}{
basicstyle=\scriptsize\bf\ttfamily,
backgroundcolor=\color{gray75},
}

\lstdefinestyle{xml}{
  language     = XML, %basicstyle=\ttfamily\footnotesize,
  morestring=[b]",
  moredelim=[s][\bfseries\color{Maroon}]{<}{\ },
  moredelim=[s][\bfseries\color{Maroon}]{</}{>},
  moredelim=[l][\bfseries\color{Maroon}]{/>},
  moredelim=[l][\bfseries\color{Maroon}]{>},
  morecomment=[s]{<?}{?>},
  morecomment=[s]{<!--}{-->},
  commentstyle=\color{comment},
  stringstyle=\color{blue},
  identifierstyle=\color{red},    
  showstringspaces=false,
  tabsize=3,
  numbers=left, % Donde se situan los numeros
  frame=single, % Se pone un marco
  backgroundcolor = \color{gray97},
  basicstyle   = \footnotesize \ttfamily,
  keywordstyle = [1]\color{keyword}\bfseries,
  keywordstyle = [2]\color{STD}\bfseries,
  breaklines=true                % sets automatic line breaking
}

\lstdefinestyle{bash}{
  language     = bash,
  tabsize=3,
  numbers=left, % Donde se situan los numeros
  frame=none, % Se pone un marco
  showstringspaces=false, % No muestre el cuadrado en los espacios de los strings
  backgroundcolor = \color{gray97},
  basicstyle   = \footnotesize \ttfamily,
  keywordstyle = [1]\color{keyword}\bfseries,
  keywordstyle = [2]\color{STD}\bfseries,
  breaklines=true,                % sets automatic line breaking
  commentstyle = \color{comment}
}

\lstdefinestyle{sql}{
  language     = sql,
  tabsize=3,
  numbers=left, % Donde se situan los numeros
  frame=none, % Se pone un marco
  showstringspaces=false, % No muestre el cuadrado en los espacios de los strings
  backgroundcolor = \color{gray97},
  basicstyle   = \footnotesize \ttfamily,
  keywordstyle = [1]\color{keyword}\bfseries,
  keywordstyle = [2]\color{STD}\bfseries,
  breaklines=true,                % sets automatic line breaking
  commentstyle = \color{comment}
}

\lstdefinestyle{json}{
  tabsize=3,
  numbers=left, % Donde se situan los numeros
  frame=none, % Se pone un marco
  showstringspaces=false, % No muestre el cuadrado en los espacios de los strings
  backgroundcolor = \color{gray97},
  basicstyle   = \footnotesize \ttfamily,
  keywordstyle = [1]\color{keyword}\bfseries,
  keywordstyle = [2]\color{STD}\bfseries,
  breaklines=true,                % sets automatic line breaking
  commentstyle = \color{comment}
}

\lstdefinestyle{file}{
  tabsize=3,
  numbers=left, % Donde se situan los numeros
  frame=none, % Se pone un marco
  showstringspaces=false, % No muestre el cuadrado en los espacios de los strings
  basicstyle   = \footnotesize \ttfamily,
  keywordstyle = [1]\color{keyword}\bfseries,
  keywordstyle = [2]\color{STD}\bfseries,
  breaklines=true,                % sets automatic line breaking
  commentstyle = \color{comment}
}


%% Bibliografy
\bibliographystyle{unsrt}


%% Code samples
\newenvironment{codefont}
{
  \begin{footnotesize}
    \begin{sffamily}
    }
    {
    \end{sffamily}
  \end{footnotesize}
}

% Examples in a box
\newenvironment{codebox}[1]
{
  \begin{Sbox}
    \begin{minipage}{#1}
      \begin{codefont}
      }
      {
      \end{codefont}
    \end{minipage}
  \end{Sbox}
  \fbox{\TheSbox}
}

% Examples in a box with no frame
\newenvironment{codenobox}[1]
{
  % \rule{15cm}{.1mm}
  \rule{\linewidth}{.1mm}
  \begin{minipage}{#1}
    \begin{normalfont}
      \begin{codefont}
        \begin{tabbing}
          -----\=-----\=-----\=-----\=-----\=-----\=-----\=-----\=-----\=-----\=-----\= \kill
        }
        {
        \end{tabbing}
      \end{codefont}
    \end{normalfont}
  \end{minipage}
  \rule{\linewidth}{.1mm}
  % \rule{15cm}{.1mm}
}

% Examples in a box with no frame 2
\newenvironment{codenobox2}[1]
{
  % \rule{15cm}{.1mm}
  \rule{\linewidth}{.1mm}
  \begin{minipage}{#1}
    \begin{normalfont}
      \begin{codefont}
        \begin{tabbing}
          -----\=tipodedatooooo\=nombredevariableee\=comentarioooooo\= \kill
        }
        {
        \end{tabbing}
      \end{codefont}
    \end{normalfont}
  \end{minipage}
  \rule{\linewidth}{.1mm}
  % \rule{15cm}{.1mm}
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Redefinimos de \part para que se incluya en el índice sin numero %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\makeatletter       %Para definir \markpart como el nombre de la parte
\def\@part[#1]#2{%
   \def\partmark{#1}
   \ifnum \c@secnumdepth >-2\relax
     \refstepcounter{part}%
     \addcontentsline{toc}{part}{#1}%\thepart\hspace{1em}#1}%
   \else
     \addcontentsline{toc}{part}{#1}%
   \fi
   \markboth{}{}%
   {\thispagestyle{empty}
    \centering
    \interlinepenalty \@M
    \normalfont
    \Huge \bfseries #2\par}%
   \@endpart}
\makeatother

\makeatletter
\renewcommand\paragraph{%
   \@startsection{paragraph}{4}{0mm}%
      {-\baselineskip}%
      {.5\baselineskip}%
      {\normalfont\normalsize\bfseries\itshape}}
\makeatother
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Paquete para manejar las referencias bibliográficas.
%\usepackage{backref}
% text for backreferences, instead of the default *pages*
%\def\backref{\small Citado en la página }

%%%%%%%%%%%% REFERENCIAS CRUZADAS MÁS ELEGANTES %%%%%%%%%%%%%%%%%%%%%
%\usepackage[pagebackref]{hyperref}

% Some language options are detected by package backref.
% This affects the following macros:
%   \backrefpagesname
%   \backrefsectionsname
%   \backrefsep
%   \backreftwosep
%   \backreflastsep

%\renewcommand*{\backref}[1]{
 % default interface
 % #1: backref list
 %
 % We want to use the alternative interface,
 % therefore the definition is empty here.
%}
%\renewcommand*{\backrefalt}[4]{%
 % alternative interface
 % #1: number of distinct back references
 % #2: backref list with distinct entries
 % #3: number of back references including duplicates
 % #4: backref list including duplicates
 %\ifnum#3=1 %
  % Ha sido citado en la página %
 %\else
   %Ha sido citado en la páginas %
 %\fi
 %#4.\par
%}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\pdfminorversion=5

\usepackage{ stmaryrd }

\endinput