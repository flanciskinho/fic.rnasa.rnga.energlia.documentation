\chapter{Diseño}
\label{cap:diseno}

	En este capítulo presenta los principales elementos del diseño del sistema. La sección \ref{cap:diseno:arquitectura} describe la arquitectura de la aplicación y los principales elementos que la componen.
	
	La sección \ref{cap:diseno:modelo} presenta el modelo lógico de datos de la aplicación y, finalmente, la sección \ref{cap:diseno:aplicacion} detalla el diseño de algunos componentes de la aplicación.

\section{Arquitectura del sistema}
\label{cap:diseno:arquitectura}

	La arquitectura de la aplicación está desarrollada bajo la base de datos relacional \emph{MariaDB}, dando lugar a una fácil escalabilidad gracias a las opciones que tiene para la replicación\cite{mariaDbHighPerformance}. A pesar de esto, para las búsquedas relacionadas con texto no se emplea directamente la BD si no que se emplea el motor de búsqueda \emph{Elasticsearch}\cite{elasticsearchGuide}.
	
	La aplicación provee un servicio web bajo una arquitectura \emph{RESTful} en el lado del servidor, o \emph{back end}, creada con \emph{Spring Boot}\cite{restSpring,springWebService,springBootBook}. Mediante el componente de \emph{Spring Security} y un sistema de autenticación basada en \emph{token} de sesión sin estado asegura el acceso a la API\cite{springSecurityEssentials,rfc7519}. La aplicación de del lado cliente, o \emph{front end}, realiza las peticiones \emph{HTTP} mediante el uso de \emph{AngularJS}\cite{http,angularJsEssentials}. Los usuarios pueden realizar peticiones directamente hacía el servicio web \emph{RESTful} o usando la interfaz web.
	
	En la figura \ref{fig:ch5:architecture} se muestra la arquitectura global de la aplicación con todos sus componentes en un despliegue. En la parte de la inferior a la derecha se encuentran las BD para el almacenamiento y los motores de búsqueda \emph{Elasticsearch}. A continuación, en la parte superior derecha, se muestran los componentes del \emph{back end}. Se puede ver que la capa de servicios se comunica con los \emph{CPDs} que tiene las colas de trabajo para ejecutar las simulaciones. Por último la parte del \emph{front end} está escrita con \emph{AngularJS} para mostrarle a los usuarios una interfaz con la que usar la aplicación.

\begin{figure}[t]
	\centering
		\includegraphics[width=1.0\textwidth]{chapter5/figures/architecture.pdf}
	\caption{Arquitectura global de la aplicación.}
	\label{fig:ch5:architecture}
\end{figure}

\section{Modelo lógico de datos}
\label{cap:diseno:modelo}
% modelo lodigo de datos

	El modelo lógico de datos se ha ido creando según se ha ido evolucionando en las distintas iteracciones del proyecto y este se ha ido adaptando para cumplir las distintas historias de usuario\cite{ScrumSprint}. La acción de ir aumentando según las necesidades el modelo de datos no ha supuesto ningún problema gracias al uso de \emph{Liquibase}\cite{practicalDevOps}.

	El diagrama de entidades persistentes se puede visualizar en la figura \ref{fig:ch5:entities} y las consideradas tablas principales se pueden ver en las tablas \ref{tab:ch5:entityUser}, \ref{tab:ch5:entityDataset}, \ref{tab:ch5:entityPopulation}, \ref{tab:ch5:entitySimulation}, \ref{tab:ch5:entitySshServer}, \ref{tab:ch5:entitySshAccount}, \ref{tab:ch5:entityApplication} y \ref{tab:ch5:entityCompiledApp}.

\begin{figure}[t]
	\centering
		\includegraphics[width=1.0\textwidth]{chapter5/figures/entities.pdf}
	\caption{Modelo de entidades persistentes.}
	\label{fig:ch5:entities}
\end{figure}

\begin{table}[t]
\centering
	\begin{tabular}{|l|l|l|} \hline
	\multicolumn{3}{|c|}{\sf \bfseries User} \\ \hline \hline
	{\sf \bfseries Campo} & {\sf \bfseries T. Dato} & {\sf \bfseries Descripción} \\ \hline
    id & UUID & (\textit{Primary Key}) Id. de \emph{User} \\ \hline 
    login & TEXT & Nombre de la cuenta de usuario \\ \hline
    password & TEXT & Contraseña encriptada en \textit{blowfish}\cite{blowfish} \\ \hline
    firstname & TEXT & Nombre propio \\ \hline
    lastname & TEXT & Apellidos \\ \hline
    email & TEXT & Correo electónico \\ \hline
    activated & BOOLEAN & Usuario activo o inactivo \\ \hline
    lang\_key & TEXT & Idioma de usuario, por defecto inglés \\ \hline
    reset\_key & TEXT & Clave para reinicio de contraseña \\ \hline
    reset\_date & TIMESTAMP & Tiempo válido para el reinicio \\ \hline
    authorities & SET TEXT & Lista de roles, por defecto usuario \\ \hline
	\end{tabular}
	\caption{Tabla de Usuarios}
	\label{tab:ch5:entityUser}
\end{table}

\begin{table}[t]
\centering
	\begin{tabular}{|l|l|l|} \hline
	\multicolumn{3}{|c|}{\sf \bfseries Dataset} \\ \hline \hline
	{\sf \bfseries Campo} & {\sf \bfseries T. Dato} & {\sf \bfseries Descripción} \\ \hline
    id & UUID & (\textit{Primary Key}) Id. de \emph{Dataset} \\ \hline 
    name & TEXT & Nombre que le ha dado el usuario\\ \hline
    description & TEXT & Descripción dada por el usuario \\ \hline
    timestamp & TIMESTAMP & Instante en el que se almacenó en el sistema\\ \hline
    numPattern & INTEGER & Número de patrones \\ \hline
    inputFeature & INTEGER & Número de características de entrada\\ \hline
    outputFeature & INTEGER & Número de características de salida\\ \hline
    numCombination & INTEGER & Número de combinaciones \\ \hline
    numClass & INTEGER & Si es para clasificación indica el número de clases \\ \hline
    belong & UUID & (\textit{Foreign key}) Id de usuario\\ \hline
	\end{tabular}
	\caption{Tabla de \emph{Dataset}}
	\label{tab:ch5:entityDataset}
\end{table}

\begin{table}[t]
\centering
	\begin{tabular}{|l|l|l|} \hline
	\multicolumn{3}{|c|}{\sf \bfseries Population} \\ \hline \hline
	{\sf \bfseries Campo} & {\sf \bfseries T. Dato} & {\sf \bfseries Descripción} \\ \hline
    id & UUID & (\textit{Primary Key}) Id. de \emph{Population} \\ \hline 
    name & TEXT & Nombre que le ha dado el usuario\\ \hline
    description & TEXT & Descripción dada por el usuario \\ \hline
    timestamp & TIMESTAMP & Instante en el que se almacenó en el sistema\\ \hline
    numIndividual & INTEGER & Número de individuos \\ \hline
    numCombination & INTEGER & Número de combinaciones \\ \hline
    sizeGenotype & INTEGER & Tamaño del genotipo del individuo \\ \hline
    belong & UUID & (\textit{Foreign key}) Id de \emph{user}\\ \hline
	\end{tabular}
	\caption{Tabla de población}
	\label{tab:ch5:entityPopulation}
\end{table}

\begin{table}[t]
\centering
	\begin{tabular}{|l|l|l|} \hline
	\multicolumn{3}{|c|}{\sf \bfseries Simulation} \\ \hline \hline
	{\sf \bfseries Campo} & {\sf \bfseries T. Dato} & {\sf \bfseries Descripción} \\ \hline
    id & UUID & (\textit{Primary Key}) Id. del \emph{Simulation} \\ \hline 
    name & TEXT & Nombre que le ha dado el usuario\\ \hline
    description & TEXT & Descripción dada por el usuario \\ \hline
    timestamp & TIMESTAMP & Instante en el que se almacenó en el sistema\\ \hline
    taskTotal & INTEGER & Número de tareas de la simulación \\ \hline
    taskDone & INTEGER & Número de tareas finalizadas \\ \hline
    taskFail & INTEGER & Número de tareas que finalizaron con error \\ \hline
    taskQueue & INTEGER & Número de tareas en cola \\ \hline
    taskServer & INTEGER & Número de tareas en el servidor \\ \hline
    belong & UUID & (\textit{Foreign key}) Id de \emph{User}\\ \hline
    use & UUID & (\textit{Foreign key}) Id de \emph{Dataset}\\ \hline
    exe & UUID & (\textit{Foreign key}) Id de \emph{Application}\\ \hline
    problem & UUID & (\textit{Foreign key}) Id de \emph{ProblemType}\\ \hline
    minimize & UUID & (\textit{Foreign key}) Id de \emph{ErrorType}\\ \hline
    launch & UUID & (\textit{Foreign key}) Id de \emph{SshServer}\\ \hline
	\end{tabular}
	\caption{Tabla de simulación}
	\label{tab:ch5:entitySimulation}
\end{table}

\begin{table}[t]
\centering
	\begin{tabular}{|l|l|l|} \hline
	\multicolumn{3}{|c|}{\sf \bfseries SpecificSimulation} \\ \hline \hline
	{\sf \bfseries Campo} & {\sf \bfseries T. Dato} & {\sf \bfseries Descripción} \\ \hline
    id & UUID & (\textit{Primary Key}) Id. de \emph{SpecificSimulation} \\ \hline 
    filelog & BLOB & Fichero de log generado\\ \hline
    jobid & UUID & Identificador de la tarea en el CPD \\ \hline
    belong & UUID & (\textit{Foreign key}) Id de \emph{SpecificSimulation}\\ \hline
    useData & UUID & (\textit{Foreign key}) Id de \emph{Dataset}\\ \hline
    launchBy & UUID & (\textit{Foreign key}) Id de \emph{SshAccount}\\ \hline
	\end{tabular}
	\caption{Tabla de simulación específica}
	\label{tab:ch5:entitySpecificSimulation}
\end{table}

\begin{table}[t]
\centering
	\begin{tabular}{|l|l|l|} \hline
	\multicolumn{3}{|c|}{\sf \bfseries SshServer} \\ \hline \hline
	{\sf \bfseries Campo} & {\sf \bfseries T. Dato} & {\sf \bfseries Descripción} \\ \hline
    id & UUID & (\textit{Primary Key}) Id. de \emph{SshServer} \\ \hline 
    dnsname & TEXT & Nombre dns o ip del servidor\\ \hline
    port & INTEGER & Número de puerto donde está el servidor ssh \\ \hline
    fingerprint & TEXT & \emph{fingerprint} del servidor \\ \hline
    maxJob & INTEGER & Número máximo de trabajos que puede tener en cola \\ \hline
    maxProc & INTEGER & Número máximo de cores que puede usar un trabajo \\ \hline
    maxTime & LONG & Tiempo máximo en minutos que un trabajo puede estar ejecutarse \\ \hline
    maxMemory & LONG & Memoria máxima que puede usar el trabajo \\ \hline
    activated & BOOLEAN & Indica si un servidor está habilitado \\ \hline
    maxConnection & INTEGER & Conexiones máximas que el \emph{back end} abrirá \\ \hline
    numConnection & INTEGER & Número de conexiones abiertas en el \emph{back end} \\ \hline
    queueType & UUID & (\textit{Foreign key}) Id de \emph{QueueType} \\ \hline
	\end{tabular}
	\caption{Tabla de servidor ssh}
	\label{tab:ch5:entitySshServer}
\end{table}

\begin{table}[t]
\centering
	\begin{tabular}{|l|l|l|} \hline
	\multicolumn{3}{|c|}{\sf \bfseries SshAccount} \\ \hline \hline
	{\sf \bfseries Campo} & {\sf \bfseries T. Dato} & {\sf \bfseries Descripción} \\ \hline
    id & UUID & (\textit{Primary Key}) Id. de \emph{SshAccount} \\ \hline 
    username & TEXT & Login del usuario \\ \hline
    key & TEXT & Clave encriptada de la clave privada \\ \hline
    salt & TEXT & \emph{Salt} empleado en \emph{key} \\ \hline
    numJob & INTEGER & Número de trabajos en está cola \\ \hline
    activated & BOOLEAN & Indica si la cuenta está activa o no \\ \hline
    belong & UUID & (\textit{Foreign key}) Id de \emph{SshServer} \\ \hline
	\end{tabular}
	\caption{Tabla de la cuenta del servidor ssh}
	\label{tab:ch5:entitySshAccount}
\end{table}

\begin{table}[t]
\centering
	\begin{tabular}{|l|l|l|} \hline
	\multicolumn{3}{|c|}{\sf \bfseries Application} \\ \hline \hline
	{\sf \bfseries Campo} & {\sf \bfseries T. Dato} & {\sf \bfseries Descripción} \\ \hline
    id & UUID & (\textit{Primary Key}) Id. de \emph{Application} \\ \hline 
    name & TEXT & Nombre de la aplicación \\ \hline
    timestamp & TIMESTAMP & Momento en el que se dió de alta \\ \hline
    activated & BOOLEAN & Indica si la aplicación está activa o no \\ \hline
	\end{tabular}
	\caption{Tabla de la aplicación}
	\label{tab:ch5:entityApplication}
\end{table}


\begin{table}[t]
\centering
	\begin{tabular}{|l|l|l|} \hline
	\multicolumn{3}{|c|}{\sf \bfseries CompiledApp} \\ \hline \hline
	{\sf \bfseries Campo} & {\sf \bfseries T. Dato} & {\sf \bfseries Descripción} \\ \hline
    id & UUID & (\textit{Primary Key}) Id. de \emph{CompiledApp} \\ \hline 
    executable & BLOB & Aplicación compilada \\ \hline
    belong & UUID & (\textit{Foreign key}) Id de \emph{Application} \\ \hline
    use & UUID & (\textit{Foreign key}) Id de \emph{SshServer} \\ \hline
	\end{tabular}
	\caption{Tabla de la aplicación compilada}
	\label{tab:ch5:entityCompiledApp}
\end{table}

\section{Diseño de la aplicación}
\label{cap:diseno:aplicacion}
% Diseno de algunos componentes de la aplicacion

	En esta sección se describe el diseño de los principales componentes de la arquitectura de la aplicación. Para crear la aplicación del lado del servidor se utilizan tecnologías de \emph{Java Spring Boot}\cite{springBootBook}. El lado cliente utiliza \emph{AngularJS} para realizar las peticiones contra el servidor y muestra la vista en formato \emph{responsive}\cite{angularJsEssentials,bootstrap}.

\clearpage
	
\subsection{Modelo}

	En esta sección se muestran los detalles de diseño e implementación de la capa de la aplicación.
	
\subsubsection{Clases persistentes}

	En la figura \ref{fig:ch5:entities} se muestra un diagrama de las entidades persistentes.
	
\subsubsection{Diseño de la capa de acceso a datos}

	La capa de acceso a datos se ha usado apoyándose en las interfaces que nos da \emph{Spring JPA} para poder usar el patrón repositorio \cite{springDataJpa}. En la figura \ref{fig:ch5:basicRepository} se muestran como son estas interfaces de las que extienden todas las interfaces de la capa de acceso a datos.

\begin{figure}[t]
	\centering
		\includegraphics[width=0.7\textwidth]{chapter5/figures/basicRepository.pdf}
	\caption{Interfaces que proporciona \emph{Spring JPA} para usar los repositorios.}
	\label{fig:ch5:basicRepository}
\end{figure}

	En las figuras \ref{fig:ch5:populationRepository}, \ref{fig:ch5:appServerRepository}, \ref{fig:ch5:userDatasetRepository} y \ref{fig:ch5:simulationRepository} se muestran las interfaces de las capas de acceso a los datos. Aunque no esté puesto en las figuras todas estas interfaces extienden de la interfaz \emph{JpaRepository} que se muestra en la figura \ref{fig:ch5:basicRepository}.

\begin{figure}[t]
	\centering
		\includegraphics[width=0.7\textwidth]{chapter5/figures/populationRepository.pdf}
	\caption{Interfaces de los repositorios relacionados con la población.}
	\label{fig:ch5:populationRepository}
\end{figure}

\begin{figure}[t]
	\centering
		\includegraphics[width=0.7\textwidth]{chapter5/figures/appServerRepository.pdf}
	\caption{Interfaces de los repositorios relacionados con la aplicación y el servidor.}
	\label{fig:ch5:appServerRepository}
\end{figure}

\begin{figure}[t]
	\centering
		\includegraphics[width=0.7\textwidth]{chapter5/figures/userDatasetRepository.pdf}
	\caption{Interfaces de los repositorios relacionados con el usuario y el \emph{dataset}.}
	\label{fig:ch5:userDatasetRepository}
\end{figure}

\begin{figure}[t]
	\centering
		\includegraphics[width=1.0\textwidth]{chapter5/figures/simulationRepository.pdf}
	\caption{Interfaces de los repositorios relacionados con la simulation.}
	\label{fig:ch5:simulationRepository}
\end{figure}

\clearpage
	
\subsubsection{Diseño de los Servicios}

	En la figura \ref{fig:ch5:serviceMethod} se muestran las clases de los servicios y en la figura \ref{fig:ch5:serviceDiagram} se muestra un diagrama de clases más global para ver las relaciones que tienen los servicios con la capa de acceso a datos.
	
\begin{figure}[t]
	\centering
		\includegraphics[width=1.0\textwidth]{chapter5/figures/serviceMethod.pdf}
	\caption{Servicios de la aplicación.}
	\label{fig:ch5:serviceMethod}
\end{figure}

\begin{figure}[t]
	\centering
		\includegraphics[width=1.0\textwidth]{chapter5/figures/serviceDiagram.pdf}
	\caption{Diagrama con los servicios de la aplicación.}
	\label{fig:ch5:serviceDiagram}
\end{figure}

\clearpage

\subsection{API REST}

	La capa \emph{Resources} que se ve en la figura \ref{fig:ch5:architecture} es gestionada por \emph{Spring MVC} para crear una \emph{API RESTful}\cite{springMvc,restSpring}. Cada operación de la \emph{API} llama a servicios de la capa modelo y las clases de la capa de acceso a datos (\emph{Repository}) a través de las entidades. Los métodos reciben y envían objetos \emph{DTO} que representan a las entidades persistentes de la capa modelo. Los \emph{DTOs} son mapeados de manera automática mediante el uso de \emph{mapstruct}\cite{mapstruct}. Los métodos \emph{REST} devuelven los objetos en formato \emph{JSON} junto con una cabecera indicando información adicional: datos de paginación, siguiente lista de elementos o número total\cite{json}.
	
	En la figura \ref{fig:ch5:restLevel} se muestran los distintos niveles de madurez que podría tener el \emph{API}.

\begin{figure}[t]
	\centering
		\includegraphics[width=0.6\textwidth]{chapter5/figures/restLevel.png}
	\caption{Niveles de madurez del servicio \emph{REST}.}
	\label{fig:ch5:restLevel}
\end{figure}	
	
	
	
\subsection{Front end}

	La vista está generada con \emph{AngularJS} y \emph{Bootstrap}\cite{angularJsEssentials,angularJsEssentials}. \emph{AngularJs} llama a la \emph{API RESTful} del servidor mediante la librería \emph{AngularJS-Resource}\cite{angularResource}. En \emph{AngularJS} se define un modelo de la vista con los datos de las entidades descritas como objetos \emph{JSON}\cite{json}. Los controladores de \emph{AngularJS} interactúan  entre la vista y el modelo para llamar al servido y resolver las peticiones del usuario usando peticiones \emph{HTTP} sobre el \emph{API RESTful}\cite{http,restSpring}.