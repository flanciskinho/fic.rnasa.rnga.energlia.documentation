\chapter{Implementación y pruebas}
\label{cap:implementacionPruebas}

\section{Implementación}

	En este apartado se muestran algunos de los detalles más importantes que se han tenido en cuenta durante la implementación del proyecto.
	
\subsection{JWT}

	Existen varios sistemas de autenticación en una aplicación web. Una de las más utilizadas es la autenticación en servidor siendo este quien almacena en una sesión la información del usuario, lo que necesitabamos persistir la información de alguna manera. Esto supone una pérdida de escalabilidad en la aplicación, ya que es el \emph{back end} el que almacena un registro por cada vez que el usuario se autentica en el sistema. 
	
	Por ello una de las nuevas tendencias en cuanto al desarrollo web moderno se refiere, es la autenticación por medio de \emph{tokens} y que el \emph{back end} sea una \emph{API RESTful} sin información de estado, o lo que es lo mismo, que sea \emph{stateless}.
	
	El funcionamiento de esta técnica es que el usuario se autentique en una aplicación, bien con el par usuario y contraseña, o usando un proveedor. Desde ese momento cada petición HTTP que haga el usuario va acompañada de un \emph{token} en la cabecera. Este \emph{token} no es más que una firma cifrada que permite a la \emph{API} identificar al usuario. Pero este \emph{token} no se almacena en el servidor, si no en el lado del cliente, por ejemplo en el \emph{localStorage} o \emph{sessionStorage}, y el \emph{API} es el que se encarga de descifrar ese \emph{token} y redirigir el flujo de la aplicación en un sentido u otro.
	
	Como los \emph{tokens} son almacenados en el lado del cliente, no hay información de estado y la aplicación se vuelve escalable. Además, nos añade una capa de seguridad porque no emplea \emph{cookies} para almacenar la información del usuario y evitamos ataques \emph{Cross-Site Request Forgery} que manipulen la sesión que envía al \emph{back end}\cite{webExploits}. Habitualmente como medida de seguridad los \emph{tokens} expiran después de un tiempo.
	
	Uno de los estándares para este tipo de autenticación es utilizar \emph{JWT}\cite{rfc7519}. El formato de un \emph{JWT} está compuesto por 3 cadenas de caracteres separadas por un punto. Un ejemplo de esto sería: 

\begin{lstlisting}[style=bash]
eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiI1NGE4Y2U2MThl
OTFiMGIxMzY2NWUyZjkiLCJpYXQiOiIxNDI0MTgwNDg0IiwiZXhwIjoiMTQyN
TM5MDE0MiJ9.yk4nouUteW54F1HbWtgg1wJxeDjqDA_8AhUPyjE5K0U  
\end{lstlisting}		

	Cada parte de la cadena de texto significa una cosa distinta.
	
	El \emph{Header}, que es la primera parte de la cabecera del \emph{token}, tiene a su vez dos partes, el tipo que en este caso es \emph{JWT} y la codificación utilizada que comúnmente es el algoritmo \emph{HMAC SHA256}\cite{sha}. El contenido  sin codificar es el siguiente:

\begin{lstlisting}[style=json]
{
	"typ": "JWT",
	"alg": "HS256"
}
\end{lstlisting}		

	El \emph{Payload} está compuesto por los llamados \emph{JWT Claims} donde irán colocados los atributos que definen nuestro token. Existen varios y los más comunes son:
\begin{itemize}
	\item \emph{sub}: Identifica el sujeto del token, por ejemplo un identificador del usuario.
	\item \emph{iat}: Identifica la fecha de creación del \emph{token}, válido para poder ponerle una fecha de caducidad.
	\item \emph{exp}: Identifica la fecha de expiración del \emph{token}.
\end{itemize}

	También se le pueden añadir más campos, incluso personalizados, como puede ser, por ejemplo, el rol de usuario.

\begin{lstlisting}[style=json]
{
	"sub": "54a8ce618e91b0b13665e2f9",
	"iat": "1424180484",
	"exp": "1425390142"
	"admin": true,
	"role": 1
}
\end{lstlisting}	

	La \emph{Signature}, que es la firma y es la última parte del \emph{JWT} está formada por los anteriores componentes, \emph{Header} y \emph{Payload}, cifrados en \emph{Base64} con una clave secreta que que está almacenada en el backend. Así sirve de \emph{Hash} para comprobar que todo está bien.
	
\begin{lstlisting}[style=bash]
HMACSHA256(
	base64UrlEncode(header) + "." +
	base64UrlEncode(payload), secret
);
\end{lstlisting}	

\subsection{Ejecución de tareas}

	Cuando se necesita hacer uso de aplicaciones de HPC es habitual usar un servidor con un sistema de colas. La razón por la que se usa el sistema de colas es para asegurarte de que estás usando unos recursos determinados del sistema de manera exclusiva. El sistema de colas es el que se encarga de poner en ejecución las tareas de manera ordenada. Es habitual, que en los CPD con un sistema de colas,  los usuarios solo puedan poner un número limitado de tareas en la cola. Esto se hace así para evitar que un usuario colapse todo el sistema.
	
	La idea de este proyecto es poder ignorar la limitación del número de tareas que se pueden meter en la cola. Para eso se crea una cola virtual que almacenará los procesos que se quieran ejecutar y esta cola virtual será la encargada de ir introduciendo las tareas en la cola del CPD para que más adelante se ejecuten en el CPD.
	
	En ocasiones los administradores de los CPDs te pueden otorgar algunos privilegios a mayores como puede ser el aumento de tareas que se puedes meter en cola pero, estos privilegios suelen ser durante un periodo de tiempo. Lo que suelen hacer es dejarte usar alguna cuenta más para que puedas hacer distintas pruebas.
	
	La idea de este proyecto es poder usar todas las cuentas que te puedan proporcionar los administradores de los CPDs y usarlas de manera eficiente para poder usar de la mejor manera los recursos de los que se disponen. Para ello la cola virtual deberá repartir los trabajos entre las distintas cuentas.
	
	Una de las opciones más fáciles para implementar es enviar todas las tareas a una cuenta y una vez que esté llena la cola de esa cuenta pasar a la siguiente. Esta opción no es muy buena idea, porque algunos CPDs tienen como limitación de que el número de trabajos que puedes tener en ejecución es mucho más pequeño que los puedes tener en cola. Para evitar este problema, se podría ir repartiendo los procesos de uno en uno por las distintas cuentas. El problema de esta opción es que se pasa mucho tiempo abriendo sesiones hacia el CPD para enviar los tareas a la cola.
	
	La opción escogida ha sido una mezcla de las dos opciones. La idea es repetir el proceso de escoger la cuenta que menos tareas tenga en cola y enviar en esa cuenta un pequeño grupo de trabajos. Con esto hacemos que todas las cuentas tengan todas un número similar de trabajos en su colas y se evita perder mucho tiempo al no abrir y cerrar tantas conexiones contra el CPD.
	
	En la figura \ref{fig:ch6:virtualQueue} se puede ver un diagrama de la idea propuesta.
	
\begin{figure}[t]
	\centering
		\includegraphics[width=0.8\textwidth]{chapter6/figures/virtualQueue.pdf}
	\caption{Idea en la que se basa la cola virtual.}
	\label{fig:ch6:virtualQueue}
\end{figure}

\subsection{Almacenar la clave privada}

	Una de las labores que tiene que hacer la aplicación es comunicarse con los CPD para poder realizar las tareas de enviar procesos a cola, mirar el estado de las colas o traer los resultados de la simulación entre otras acciones. Para eso necesitamos almacenar el usuario y la clave de autenticación. El método de autenticación que se usa con los servidores es mediante el par de claves públicas y privadas.
	
	La clave privada de la cuenta de usuario es un dato muy sensible y no se debería almacenar como texto plano en la BD. Por eso es necesario cifrarlo, pero hay que escoger un método de cifrado que permita descifrar el contenido. El método de cifrado que se ha escogido ha sido AES de 256 bytes de clave, con un vector de inicialización de tamaño 16 y con al que se le añade \emph{salt} y se repite durante 32 iteracciones\cite{aes}.
	
	Una vez escogido el método y los parámetros que se van a usar de este, queda por escoger la contraseña para cifrar. Se podría optar por usar para cifrar todas las claves privadas, pero no es buena idea porque si alguien se hace con la contraseña y consiguiese acceso a la BD podría robar todas las claves privadas. Una forma para evitar usar la misma clave para todo es usar algún campo de ese tupla de la base de datos como clave, pero el problema está en que si alguien logra acceso a la BD se le podría ocurrir probar a descifrar mediante los campos de la tupla.
	
	La opción final como clave de cifrado para AES es dividir la clave en dos partes la primera de ellas es la clave compartida y la segunda está formada por la combinación de campos formados por la tupla de la cuenta de usuario y la tupla del servidor a la que esta pertenece.

\subsection{Generación de informes}

	Como hemos mencionado en el capítulo \ref{cap:estadoArte}, cada vez que se lanza una simulación se prueba con varias combinaciones de \emph{dataset} y varias combinaciones de poblaciones. Si suponemos que tenemos ocho combinaciones de \emph{dataset} y diez de poblaciones tendremos ochenta ejecuciones. Dicho de otra manera tendremos ochenta logs para el conjunto de entrenamiento y si el \emph{dataset} tiene particiones para validación y test serían ochenta logs más para cada una de las particiones. Es decir podríamos tener 240 logs para analizar y a mayores hay que multiplicar por las generaciones del AG para tener los registros necesarios, lo que supone que si lanzamos el AG con 7000 generaciones tendríamos 1680000 registros para mostrar al usuario. Mover tal cantidad de datos hasta el usuario final puede suponer un problema de rendimiento o darle al usuario la sensación de que la aplicación no funciona como debería.
	
	Por la razón mencionada anteriormente, se ha decidido simplificar los datos que se envían al usuario. No se envían 1680000 datos para dibujar las gráficas, que además supondría dibujar 80 series. Se ha optado por mostrar como mucho 4 gráficas, una general donde se muestra la evolución global de los 80 logs de la partición de entrenamiento, de la de validación y la de test. A mayores para cada partición se muestra la evolución global de los 80 del error y la desviación media.
	
	Con lo propuesto anteriormente evitamos enviar las 80 series y enviamos como mucho 6 series, una para el error global de cada partición y otra para la desviación media de cada partición. Aún así, enviando solo esas 6 series, si el AG generó 7000 registros se necesitaría enviar 42000 datos. Para simplificar esta situación, se optó por enviar una pequeña cantidad de puntos es decir dividir los 7000 registros en tan sólo una pequeña cantidad. Se ha optado que sean 40 puntos equidistantes. Con esta propuesta estamos simplificando los 1680000 registros en tan sólo 240 datos, lo que en este caso supone enviar 7000x menos datos.
	
	En la figura \ref{fig:ch6:ideaChart} se muestra el concepto de simplificación de los datos.  Las líneas punteadas azules representan la generación en la que se quiere obtener el dato global. Dentro de cada serie se calcula una media ponderada de los dos puntos que lo cubren que es lo que representan las cruces. Una vez que tenemos las medias ponderadas de todas las series se calculan la media de esta y ese valor, que es la nube azul, es el punto que se emplea.
	
	En la figura \ref{fig:ch6:logChart} se muestra un ejemplo real donde se muestran las gráficas.

\begin{figure}[t]
	\centering
		\includegraphics[width=0.5\textwidth]{chapter6/figures/ideaChart.pdf}
	\caption{Concepto de simplificación de los datos.}
	\label{fig:ch6:ideaChart}
\end{figure}

\begin{figure}[t]
	\centering
		\includegraphics[width=1.0\textwidth]{chapter6/figures/logChart.png}
	\caption{Ejemplo de las gráficas de la evolución del error.}
	\label{fig:ch6:logChart}
\end{figure}

\section{Pruebas}

\subsection{Pruebas unitarias}

	Para el entorno de pruebas unitarias se emplea el \emph{framework} de \emph{JUnit}\cite{junit}. Las pruebas unitarias realizadas con \emph{JUnit} sirven para comprobar el éxito de acciones sobre el modelo y controladores, creando registros en las tablas del proyecto sobre la base de datos de test\cite{unitTest}.
	
	Las clases test de \emph{Java} se encuentran en la ruta \emph{/energlia/src/test/java/es/udc/tic/rnasa}.

\subsection{Pruebas de integración}

	Las pruebas de integración se realizan en el ámbito de la aplicación una vez que se han aprobado las pruebas unitarias\cite{testModel}. Consisten en verificar que un conjunto de partes de la aplicación y acciones funcionan correctamente.
	
	Durante el proceso de creación del proyecto se han realizado las pruebas de las acciones sobre los recursos que proporcionan el \emph{API RESTful}. Las pruebas de los servicios web se generan usando el \emph{framework} \emph{Mockito}\cite{mockito}.
	
	Las pruebas del \emph{front end} se realizaron apoyándose en \emph{KarmaJS}\cite{karma}.
	

\subsection{Pruebas de estrés}
%https://www.digitalocean.com/community/tutorials/how-to-measure-mysql-query-performance-with-mysqlslap
%http://www.techrepublic.com/blog/how-do-i/how-do-i-stress-test-mysql-with-mysqlslap/
	Las pruebas de estrés de \emph{MariaDb} se han realizado con la herramienta \emph{mysqlslap}. Para ello vamos a probar a paralelizar las consultas como si se trataran de varios usuarios accediendo simultaneamente. Para ello vamos abrir 50 conexiones y lanzar desde 50 consultas hasta 2000 consultas lo que quiere decir que cada cliente ejecutará desde 1 hasta las 40 consultas. Además le indicamos que el número que use cinco columnas de enteros y 20 de caracteres. El comando para ejecutar dicha orden es el siguiente:

\begin{lstlisting}[style=bash]
$ for i in `seq 50 50 2000`; do  mysqlslap --user=uprod --password=pprod --host=localhost --concurrency=50 --number-of-queries=$i --number-int-cols=5 --number-char-cols=20 --auto-generate-sql --verbose ; done
\end{lstlisting}

	En la figura \ref{fig:ch6:testStress} se puede ver el resultado obtenido de las pruebas de estrés.
	
\begin{figure}[t]
	\centering
		\includegraphics[width=0.7\textwidth]{chapter6/figures/testStress.png}
	\caption{Resultado de ejecutar mysqlslap.}
	\label{fig:ch6:testStress}
\end{figure}

\subsection{Pruebas de compatibilidad}

	La aplicación es totalmente funcional en los navegadores \emph{Google Chrome} y \emph{Firefox}. En algunos casos se pueden apreciar pequeños cambios en la forma de representar objetos HTML, según las opciones de las hojas de estilos pero en ningún caso afectando a la funcionalidad y rendimiento de la aplicación.


\section{Herramientas utilizadas}

\subsection{Navicat Premiun}

	Para la ver la configuración de la base de datos poder ver que los esquemas se creaban acorde a lo especificado en \emph{Liquibase} se ha usado \emph{Navicat Premiun} que es un sistema para administrar diferentes SGBD\cite{navicat}.

\subsection{Omnigraffle Professional}

	Para la creación de diagramas tanto de arquitectura como los diagramas de clases entre otros se ha empleado la herramienta \emph{Omnigraffle Pro}\cite{omnigraffle}.

\subsection{Omniplan}

	\emph{Omniplan} es una aplicación para la gestión de proyectos\cite{omniplan}. Se utiliza para la planificación del proyecto junto con la definición de las iteracciones más representativas del proyecto en hitos y su visualización en diagramas de \emph{Gantt}\cite{gantt}.

\subsection{IntelliJ Idea}

	Para el desarrollo de la aplicación se ha utilizado el \emph{IDE IntelliJ Idea} con la licencia gratuita para estudiantes\cite{intellijIdea}.

\subsection{Git}

	Se ha utilizado \emph{Git} como control de versiones y se ha empleado el repositorio de \emph{Bitbucket} para mantener los cambios entre versiones de la aplicación\cite{gitBook,bitbucket}.